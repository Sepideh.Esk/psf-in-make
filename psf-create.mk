# Final target.
all: final

# Make all the commands in the recipe in one shell.
.ONESHELL:

# Second expantion.
.SECONDEXPANSION:

# Include configure files.
include config/*.conf

# Include built configuration file.
include $(foreach r,$(mag-range),$(bdir)/$(r)/*.conf)




# Make stamp of stars.
sdir=$(foreach s,$(mag-range),$(bdir)/$(s)/stamps)
$(sdir):; mkdir $@
stamp=$(foreach s,$(mag-range), \
       $(foreach i,$(image-id), \
        $(foreach c,$($(s)-$(i)-stars), \
          $(bdir)/$(s)/stamps/$(i)-$(c).fits)))
$(stamp): $(bdir)/%.fits: $(segdir)/$$(word 3,$$(subst -, ,$$(subst /, ,%))).fits \
          config/stamp-width-$$(word 1,$$(subst /, ,%)).conf \
          config/directories.conf \
          | $(segdir) $(sdir)

#	Extract the image id and coordinate and 'stampwidth' for each
#	star.
	imgid=$(word 3,$(subst -, ,$(subst /, ,$*)))
	coordinate=$($(subst /,-,$(subst stamps/,,$*)-center))
#	stamp-width=stamp-width-$(word 1,$(subst /, ,$*))


#	Make stamp of stars.
	astscript-psf-stamp $(segdir)/$$imgid.fits \
	         --mode=wcs --stampwidth=$(stamp-width-$(word 1,$(subst /, ,$*))) \
	         --tmpdir=$(subst .fits,-stamp,$@) \
	         --center=$$coordinate --normradii=20,30 \
	         --segment=$(segdir)/$$imgid.fits --output=$@




# Make stack of different part of the PSF.
stack=$(foreach m,$(mag-range),$(bdir)/$(m)/stack.fits)
$(stack): $(bdir)/%/stack.fits: \
          $$(foreach i,$$(image-id), \
            $$(foreach c,$$(%-$$(i)-stars), \
              $$(bdir)/%/stamps/$$(i)-$$(c).fits)) \
          config/directories.conf

#	Find number of images for making stack of stars.
	imgs="$(wildcard $(bdir)/$*/stamps/*.fits)"
	numimgs=$$(echo $$imgs | wc -w)

#	Stack stars.
	astarithmetic $$imgs $$numimgs 3 0.2 sigclip-mean -g1 \
	              --output=$@ --wcsfile=none




# join different part of the PSF.
psf=$(bdir)/psf.fits
$(psf): $(stack) config/scale-factor.conf config/psf-unite.conf | $(bdir)

#	Compute the scale factor.
	scale=$$(astscript-psf-scale-factor $(bdir)/outer/stack.fits \
	                  --psf=$(bdir)/inner/stack.fits --center=$(center) \
	                  --mode=img --normradii=$(normradii) --quiet)

#	Make PSF.
	astscript-psf-unite $(bdir)/outer/stack.fits \
	          --inner=$(bdir)/inner/stack.fits --radius=$(radius) \
	          --scale=$$scale --output=$@

#	Get S/N level of the final PSF in relation to the actual data.
	numouter=$$(ls $(bdir)/outer/stamps/*.fits | wc -l)
	numinner=$$(ls $(bdir)/inner/stamps/*.fits | wc -l)

#	Add S/N level as keyword to header's of PSF.
	astfits $@ --write=/,"PSF properties" \
	        --write=NUMOUTER,$$numouter,"Number of images for outer part." \
	        --write=NUMINNER,$$numinner,"Number of images for inner part."


# Final target.
final: $(psf)
	echo "PSF has been created."
