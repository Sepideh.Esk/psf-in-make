# Final target.
all: final

# Make all the commands in the recipe in one shell.
.ONESHELL:

# Second expantion.
.SECONDEXPANSION:

# Include configure files.
include config/*.conf




# Make the build directory.
$(bdir): ; mkdir $@




# Download the images.
rawdir=$(bdir)/raw
$(rawdir): | $(bdir); mkdir $@
images=$(foreach i,$(image-id),$(rawdir)/$(i).fits.fz)
$(images): $(rawdir)/%.fits.fz: | $(rawdir)
	urlend="jplus-dr2/get_fits?id=$*"
	urlbase="http://archive.cefca.es/catalogues/vo/siap/"
	wget $$urlbase$$urlend -O $@





#  Make Gaussian kernel in the Segment directory.
$(segdir): | $(bdir); mkdir $@
kernel=$(segdir)/kernel.fits
$(kernel): | $(segdir)
	astmkprof --kernel=gaussian,2,5 --oversample=1 \
	          --output=$@





# Make Segmentation map.
seg=$(foreach s,$(image-id),$(segdir)/$(s).fits)
$(seg): $(segdir)/%.fits: $(rawdir)/%.fits.fz $(kernel) \
        config/flat-crop-section.conf config/saturation-limit.conf \
        | $(segdir)

# 	Dithering pattern.
	crop=$(subst .fits,-crop.fits,$@)
	astcrop $(rawdir)/$*.fits.fz --section=$(flat-crop-section) \
	        --mode=img --output=$$crop

# 	Mask saturation pixels.
	msat=$(subst .fits,-masked-sat.fits,$@)
	astarithmetic $$crop set-i i i $(satlim) gt \
	              2 dilate 2 dilate 2 dilate 2 dilate \
	              nan where --output=$$msat

# 	Convolve the image with Gaussian kernel.
	mconv=$(subst .fits,-masked-conv.fits,$@)
	astconvolve $$msat --kernel=$(kernel) --domain=spatial \
	            --output=$$mconv


#	Fill the nan pixels in the masked image.
	fill=$(subst .fits,-fill.fits,$@)
	astarithmetic $$mconv 2 interpolate-maxofregion \
	              --output=$$fill

#	Fill the  nan pixels in the convolved image.
	fconv=$(subst .fits,-fill-conv.fits,$@)
	astarithmetic $$mconv 2 interpolate-maxofregion \
	              --output=$$fconv

# 	Finde detections in image.
	nc=$(subst .fits,-nc.fits,$@)
	astnoisechisel $$fill --convolved=$$fconv \
	               --detgrowquant=0.8 --interpnumngb=100 \
	               --detgrowmaxholesize=100000 \
	               --output=$$nc

# 	Find the clumps and segment in the detection regions.
	segraw=$(subst .fits,-seg.fits,$@)
	astsegment $$nc -h1 --convolved=$$fconv \
	           --gthresh=-10 --objbordersn=0 \
	           --rawoutput --output=$$segraw

# 	Final target is an artificial Segment output:
# 	1. Sky-subtract input (from NoiseChisel but
#	   saturated pixels masked) in the first extension
# 	   (has to have a name 'INPUT-NO-SKY').
# 	2. Copy 'CLUMPS' and 'OBJECTS' from the raw Segment.
# 	3. Copy 'SKY_STD' from NoiseChisel.
	astarithmetic $$nc -hINPUT-NO-SKY $$msat -h1 \
	               isblank nan where --output=$@

	astfits $@ --update=EXTNAME,INPUT-NO-SKY

	astfits $$segraw --copy=CLUMPS --copy=OBJECTS \
	        --output=$@

	astfits $$nc --copy=SKY_STD --output=$@

# 	Clean up.
	rm $$nc $$msat $$segraw $$kernel $$mconv $$fill \
	   $$fconv $$crop




# Select stars in different range of magnitude.
rangedir=$(foreach d,$(mag-range),$(bdir)/$(d))
$(rangedir): | $(bdir); mkdir $@
range=$(foreach r, $(mag-range), \
        $(foreach i,$(image-id),$(bdir)/$(r)/$(i).conf))
$(range): $(bdir)/%.conf: $(segdir)/$$(word 2,$$(subst /, , %)).fits $(gaia) \
          config/mag-range-$$(word 1,$$(subst /, , %)).conf \
          | $(bdir)/$$(word 1,$$(subst /, , $*))

# 	Download the list of stars.
	cat=$(subst .conf,.fits,$@)
	magr=$(mag-range-$(word 1,$(subst /, , $*)))
	dist=$(mag-range-$(word 1,$(subst /, , $*))-dist)
	imgid=$(word 2,$(subst /, , $*))
	astscript-psf-select-stars --output=$$cat \
	         --mindistdeg=$$dist  \
	         --tmpdir=$(subst .conf,-select-stars,$@) \
	         $(segdir)/$$imgid.fits \
	         --magnituderange=$$magr --keeptmp

#	Write total number of stars in configuration file.
	base=$(subst /,-,$*)
	nstar=$$(astfits $$cat -h1 --keyvalue=NAXIS2 --quiet)
	stars=$$(seq $$nstar | tr '\n' ' ')
	echo "$$base-stars = $$stars" > $@.conf

# 	Information of each star in configuration file.
	counter=1
	asttable $$cat --sort=phot_g_mean_mag -cra,dec \
	         | while read -r ra dec; do
	             echo "$$base-$$counter-center = $$ra,$$dec" >> $@.conf
	             counter=$$((counter+1))
	           done
	mv $@.conf $@

#	Clean up.
	rm $$cat



# final target.
final: $(range)
	echo "PSF preparations are done!"
