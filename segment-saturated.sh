set -e


input=$1
satlim=$2
kernel=$3
ncparams="$4"
bdir=$5
output=$6




# Build output directory.
if ! [ -d  $bdir ]; then mkdir $bdir; fi


# Mask saturation pixels.
msat=$bdir/$(echo $(basename $output) \
	   | sed 's|.fits|-masked-sat.fits|')
astarithmetic $input set-i i i $satlim gt \
              2 dilate 2 dilate 2 dilate 2 dilate \
              nan where --output=$msat

# Convolve the image with Gaussian kernel.
mconv=$bdir/$(echo $(basename $output) \
	   | sed 's|.fits|-masked-conv.fits|')
astconvolve $msat --kernel=$kernel --domain=spatial \
            --output=$mconv


# Fill the nan pixels in the masked image.
fill=$bdir/$(echo $(basename $output) \
	   | sed 's|.fits|-fill.fits|')
astarithmetic $mconv 2 interpolate-maxofregion \
              --output=$fill

# Fill the  nan pixels in the convolved image.
fconv=$bdir/$(echo $(basename $output) \
	   | sed 's|.fits|-fill-conv.fits|')
astarithmetic $mconv 2 interpolate-maxofregion \
              --output=$fconv

# Find detections in image.
nc=$bdir/$(echo $(basename $output) \
	   | sed 's|.fits|-nc.fits|')
astnoisechisel $fill --convolved=$fconv \
               $ncparams --output=$nc

# Find the clumps and segment in the detection regions.
segraw=$bdir/$(echo $(basename $output) \
	   | sed 's|.fits|-seg.fits|')
astsegment $nc -h1 --convolved=$fconv \
           --gthresh=-10 --objbordersn=0 \
           --rawoutput --output=$segraw

# For considering the clump's of main star and clump's of spikes as
# one clump, the clumps are dilated and one clump for the center of
# satrs and their spikes are made. Then for putting a special number
# for each clump we have used the 'connected-components' and for
# puting the fraction regions value to -1 we have used the detections
# of the NoiseChisel.
dil=$bdir/$(echo $(basename $output) \
	   | sed 's|.fits|-dilated.fits|')
astarithmetic $nc -hDETECTIONS set-det \
              $segraw -hCLUMPS \
              0 gt 2 dilate 1 connected-components set-newclumps \
              newclumps \
	      det 1 eq \
	      newclumps 0 eq \
              and -1 where  \
              --output=$dil

# Change name.
astfits $dil --update=EXTNAME,CLUMPS


# Final target is an artificial Segment output:
# 1. Sky-subtract input (from NoiseChisel but
#   saturated pixels masked) in the first extension
#    (has to have a name 'INPUT-NO-SKY').
# 2. Copy 'CLUMPS' and 'OBJECTS' from the raw Segment.
# 3. Copy 'SKY_STD' from NoiseChisel.
astarithmetic $nc -hINPUT-NO-SKY $msat -h1 \
               isblank nan where --output=$output

astfits $output --update=EXTNAME,INPUT-NO-SKY

astfits $dil --copy=CLUMPS --output=$output

astfits $segraw --copy=OBJECTS --output=$output

astfits $nc --copy=SKY_STD --output=$output

# Clean up.
rm $nc $msat $segraw $kernel $mconv $fill \
   $fconv $crop $dil
