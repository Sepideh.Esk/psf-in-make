# Final target.
all: final

# Make all the commands in the recipe in one shell.
.ONESHELL:

# Second expantion.
.SECONDEXPANSION:

# Include configure files.
include config/*.conf






# Find stars in the field of input image which we want subtract PSF
# from that.
$(subdir): | $(bdir); mkdir $@
substars=$(foreach s,$(basename $(subtract-image-names)),$(subdir)/$(s)-gaia.fits)
$(substars): $(subdir)/%-gaia.fits: config/subtract-mag-range.conf \
             $(indir)/%.fits | $(subdir)

#	Download stars which overlap with main image which we want
#	subtract stars from it.
	astquery gaia --dataset=edr3 --overlapwith=$(indir)/$*.fits \
	         -cra,dec,phot_g_mean_mag --output=$@ \
	         --range=phot_g_mean_mag,$(subtract-mag-range) \
	         --sort=phot_g_mean_mag





# Subtract stars.
subtracted=$(subst -gaia.fits,.fits,$(substars))
$(subtracted): $(subdir)/%.fits: $(subdir)/%-gaia.fits

#	Get S/N level of the final PSF in relation to the actual data:
	snlevel=$$(astfits $(bdir)/psf.fits -h1 --keyvalue=NUMOUTER \
	                   --quiet | awk '{print sqrt($$1)}')

#	Put a copy of the image we want to subtract the PSF from in
#	the final file (this will be over-written after each
#	subtraction). We choose another name for our final target
#	('subtract'). Because if the final target exists, and we have
#	an error in the recipe and rerun Make again, we will not see
#	the error.
	subtract=$(subst .fits,-sub.fits,$@)
	cp $(indir)/$*.fits $$subtract

#	Name of log-file to keep status of the subtraction of each star.
	logname=$(subdir)/$*.log
	echo "# Column 1: RA [deg, f64] Right ascension of star." > $$logname
	echo "# Column 2: Dec [deg, f64] Declination of star." >> $$logname
	echo "# Column 3: Stat [deg, f64] Status (1: subtracted)" >> $$logname


#	Go over each item in the bright star catalog:
	asttable $(subdir)/$*-gaia.fits -cra,dec --sort=phot_g_mean_mag \
	    | while read -r ra dec; do \

#	    Put a comma between the RA/Dec to pass to options.
	    center=$$(echo $$ra $$dec | awk '{printf "%s,%s", $$1, $$2}')

#	    Calculate scale value.
	    scale=$$(astscript-psf-scale-factor $(indir)/$*.fits \
	                      --mode=wcs --quiet \
	                      --psf=$(bdir)/psf.fits \
	                      --center=$$center \
	                      --normradii=10,15 \
	                      --segment=$(indir)/$*.fits)

#	    Subtract this star if the scale factor if less than the S/N
#	    level claculate above.
	    if [ x"$$scale" = x ]; then check=bad;
	    else
	      check=$$(echo $$snlevel $$scale \
	                   | awk '{if($$1>$$2) c="good"; else c="bad"; print c}')
	    fi

	    if [ $$check = good ]; then

#	        A temporary file to subtract this star.
	        subtmp=$(subdir)/$*-tmp.fits

#	        Subtract this star from the image where all previous stars
#	        were subtracted.
	        astscript-psf-subtract $$subtract \
	                 --mode=wcs \
	                 --psf=$(bdir)/psf.fits \
	                 --scale=$$scale \
	                 --center=$$center \
	                 --output=$$subtmp

#	        Rename the temporary subtracted file to the final one:
	        mv $$subtmp $$subtract

#	        Keep the status for this star.
	        status=1
	    else

#	        Let the user know this star didn't work, and keep the status
#	        for this star.
	        echo "$$center: $$scale is larger than $$snlevel"
	        status=0
	    fi

#	    Keep the status in a log file.
	    echo "$$ra $$dec $$status" >> $$logname

	done

#	Move the subtracted image to the final target.
	mv $$subtract $@




# Subtract actual image from the PSF subtracted image.
###scatlight=



# Final target.
final: $(subtracted)
